Gem::Specification.new do |spec|
  spec.name        = 'git_rid'
  spec.version     = '1.1.0'
  spec.licenses    = ['MIT']
  spec.summary     = 'Change all instances of master branches in gitlab (where you have access)'
  spec.description = 'Change every main branch to the name of your choice instead of master (default is main)'
  spec.authors     = ['Alex Ives']
  spec.email       = 'alex@ives.dev'
  spec.files       = ['lib/git_rid.rb']
  spec.homepage    = 'https://gitlab.com/alexives/git_rid'
  spec.executables = ['git_rid']
  spec.metadata    = { 'source_code_uri' => 'https://gitlab.com/alexives/git_rid' }
  spec.add_dependency 'gitlab', '~> 4.15.0'
  spec.add_dependency 'dotenv', '~> 2.7.5'
  spec.add_dependency 'highline', '~> 2.0.3'
end
