# Git rid of your master branch

There are a variety of reasons you might want to change your default branch names. Updated convention, getting rid of git flow, avoiding racially sensitive terms. 

This gem will help you do that on gitlab! It goes through all of the projects where you have maintainer or above access and changes all of them.

## Installation

```
gem install git_rid
```

## Usage:

```
Usage: git_rid
    -b, --branch-name=main           Set the new branch name you want
    -o, --outdated-branch=master     Specify what the outdated branch name was
    -uhttps://gitlab.com/api/v4,     Specify the url for the gitlab server
        --url
    -t, --token=???                  Specify the token for the gitlab user (must be maintainer or above)
    -h, --help                       Show usage help text
```

You can specify the token as `GITLAB_API_PRIVATE_TOKEN` in your environment
