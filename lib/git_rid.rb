require 'gitlab'

class GitRid
  def self.perform!(options)
    updated_name = options[:updated]
    outdated_name = options[:outdated]

    Gitlab.configure do |config|
      config.endpoint       = options[:url] || ENV['GITLAB_API_ENDPOINT']
      config.private_token  = options[:token] || ENV['GITLAB_API_PRIVATE_TOKEN']
    end

    projects = Gitlab.projects(min_access_level: 40)

    if projects.empty?
      puts "Looks like you aren't a maintainer on any projects :("
      exit 1
    end

    projects.auto_paginate do |project|
      unless options[:yes]
        confirm = ask("Update #{project.name}? [Y/N] ") { |yn| yn.limit = 1 }
        next if confirm.downcase == 'n'
        exit unless confirm.downcase == 'y'
      end
      mb = Gitlab.branch(project.id, outdated_name)

      Gitlab.create_branch(project.id, updated_name, mb.commit.id)

      if mb.protected
        Gitlab.protect_branch(project.id, updated_name)
        Gitlab.unprotect_branch(project.id, outdated_name)
      end

      if project.default_branch == outdated_name
        Gitlab.edit_project(project.id, {default_branch: updated_name})
      end

      Gitlab.delete_branch(project.id, outdated_name)
      puts "Updated #{project.name} and replaced #{outdated_name} with #{updated_name}"
    rescue Gitlab::Error::NotFound => e
      puts "Skipping #{project.name}, it doesn't have a #{outdated_name} branch!"
    rescue Gitlab::Error::BadRequest
      puts "Skipping #{project.name} because it already had a branch called #{updated_name}... you'll have to sort that out yourself."
    end
  end
end